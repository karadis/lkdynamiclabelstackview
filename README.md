# LKDynamicLabelStackView

## Description

`LKDynamicLabelStackView` is a vertical stack of labels that they can scroll infinitely in both directions
You can customize it each list item cell with the given fields:
`startInExpandedState`
`color` : text color
`font` : text font
`insets` : the inset of each cell content

`LKDynamicLabelStackView` supports rotation.
Written in Swift 3.2

![alt text](https://i.imgur.com/28gXMXS.png)

## Requirements

iOS 8.0< 
Swift 3.2<

## Installation

Simply add the `LKDynamicLabelStackView` Folder to your project.
You then will have access to the `LKDynamicLabelStackView` classes.

## Usage

`LKDynamicLabelStackView` uses a data object called `StackItem` which the developer need to instantiate with *text* and *image* (if any),
and assign an array of Stack items to the stack View.
The view will automatically reloads and displays the content.

```
self.stackView = LKDynamicLabelStackView()
self.stackView?.frame = self.view.bounds
self.stackView?.color = UIColor.purple
self.stackView?.insets = UIEdgeInsets(top: 32, left: 16, bottom: 32, right: 16)
self.stackView?.items = [StackItem(text: "hello", imageUrl: nil), StackItem(text: "world!", imageUrl:pizza.png")
self.view.addSubview(self.stackView!)
```

## Example

The Sample project uses the `LKDynamicLabelStackView` to display song entries from the given URL from the code challange.
tried to be as creative as possible to display as much information from the data given.
The sample project uses `ObjectMapper` to Parse the objects from the URL. You may need to run `pod install` for this to work.

## TODO

CocoaPods :)

## Author

Liron Karadi, 
Hope to hear from you soon!

[lironkaradi@gmail.com](mailto:lironkaradi@gmail.com)

[https://www.linkedin.com/in/lironkaradi/](https://www.linkedin.com/in/lironkaradi/)

## License

LKDynamicLabelStackView is available under the MIT license. See the LICENSE file for more info.
