//
//  LKDynamicLabelViewCell.swift
//  eBayCodeChallange
//
//  Created by Liron Karadi on 07/02/2018.
//  Copyright © 2018 eBay. All rights reserved.
//

import UIKit

let dynamicLabelViewCellDefaultImageSize = CGSize(width: 50, height: 50)

class LKDynamicLabelViewCell: UITableViewCell {

    @IBOutlet private weak var dynamicLabel: UILabel!
    @IBOutlet private weak var dynamicImageView: UIImageView!
    
    @IBOutlet weak var labelTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var labelTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var labelLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var labelBottomConstraint: NSLayoutConstraint!
    
    var dynamicFont: UIFont?
    var dynamicColor: UIColor?
    var insets: UIEdgeInsets = UIEdgeInsetsMake(32, 16, 32, 16) {
        didSet {
            DispatchQueue.main.async {
                self.setupConstraints()
                self.layoutIfNeeded()
            }
        }
    }
    
    var item: StackItem? {
        didSet {
            self.dynamicLabel.numberOfLines = item?.expanded == true ? 0 : 3
            self.layoutIfNeeded()
        }
    }
    
    class func calculateHeight(item: StackItem, insets: UIEdgeInsets, maxWidth: CGFloat, font: UIFont?) -> CGFloat {
        
        let verticalInsets = insets.top + insets.bottom
        let horizontalInsets = (item.imageUrl != nil ? dynamicLabelViewCellDefaultImageSize.width + insets.left : CGFloat(0)) + insets.left + insets.right
        let tempLabel = UILabel(frame: CGRect(x: 0, y: 0, width: maxWidth - horizontalInsets, height: CGFloat.greatestFiniteMagnitude))
        tempLabel.numberOfLines = item.expanded == true ? 0 : 3
        tempLabel.font = font
        tempLabel.lineBreakMode = .byWordWrapping
        tempLabel.textAlignment = .natural
        tempLabel.text = item.text
        let size = tempLabel.sizeThatFits(CGSize(width: maxWidth - horizontalInsets, height: CGFloat.greatestFiniteMagnitude))
        
        let height = size.height + verticalInsets
        return item.imageUrl != nil ? max(height, dynamicLabelViewCellDefaultImageSize.height + verticalInsets) : height
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.dynamicImageView.image = nil
        self.setupConstraints()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.dynamicLabel.numberOfLines = item?.expanded == true ? 0 : 3
        self.dynamicLabel.textColor = self.dynamicColor
        self.dynamicLabel.font = self.dynamicFont
        if let url = self.item?.imageUrl {
            self.dynamicImageView.imageFromUrl(urlString: url)
        }
        self.dynamicLabel.text = item?.text
        self.dynamicLabel.sizeToFit()
        self.setupConstraints()
    }
    
    func setupConstraints() {
        self.labelTopConstraint.constant = insets.top
        self.labelBottomConstraint.constant = insets.bottom
        self.labelLeadingConstraint.constant = insets.left
        self.labelTrailingConstraint.constant = insets.right
        
        if let _ = self.item?.imageUrl {
            self.dynamicImageView.isHidden = false
            self.labelLeadingConstraint.constant = dynamicLabelViewCellDefaultImageSize.width + (2 * (self.insets.left))
        } else {
            self.dynamicImageView.isHidden = true
            self.labelLeadingConstraint.constant = self.insets.left
        }
        self.updateConstraintsIfNeeded()
    }
}
