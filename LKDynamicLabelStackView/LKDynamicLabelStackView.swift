//
//  LKDynamicLabelStackView.swift
//  eBayCodeChallange
//
//  Created by Liron Karadi on 07/02/2018.
//  Copyright © 2018 eBay. All rights reserved.
//

import UIKit

class LKDynamicLabelStackView: UIView, UITableViewDelegate, UITableViewDataSource {

    public var tableView: UITableView = UITableView()
    public var startInExpandedState: Bool = false
    
    public var color: UIColor = .black
    public var font: UIFont = UIFont.systemFont(ofSize: 14)
    public var insets: UIEdgeInsets = UIEdgeInsetsMake(32, 16, 32, 16) // defaults

    public var items: [StackItem] = [] {
        didSet {
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.layoutIfNeeded()
            }
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.setup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.setup()
    }
    
    func setup() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.rotated), name: .UIDeviceOrientationDidChange, object: nil)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.frame = self.bounds
        let bundle = Bundle(for: LKDynamicLabelViewCell.self)
        self.tableView.register(UINib(nibName: "LKDynamicLabelViewCell", bundle: bundle), forCellReuseIdentifier: "LKDynamicLabelViewCell")
        self.addSubview(self.tableView)
    }
    
    @objc func rotated() {
        if let bounds = self.superview?.bounds {
            self.tableView.frame = bounds
        }
    }
    
    //MARK: UITableView Delegates
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return LKDynamicLabelViewCell.calculateHeight(item: self.items[indexPath.row], insets: self.insets, maxWidth: self.frame.width, font: self.font)
    }

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LKDynamicLabelViewCell") as! LKDynamicLabelViewCell

        cell.insets = self.insets
        cell.dynamicColor = self.color
        cell.dynamicFont = self.font
        cell.item = self.items[indexPath.row]
        return cell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        tableView.beginUpdates()
        self.items[indexPath.row].expanded = !self.items[indexPath.row].expanded
        tableView.endUpdates()
    }
}
