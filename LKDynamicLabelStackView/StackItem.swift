//
//  StackItem.swift
//  eBayCodeChallange
//
//  Created by Liron Karadi on 08/02/2018.
//  Copyright © 2018 eBay. All rights reserved.
//

import UIKit

class StackItem: NSObject {
    public var text: String?
    public var imageUrl: String?
    public var expanded: Bool = false
    
    convenience public init(text: String?, imageUrl: String?, expanded: Bool = false) {
        self.init()
        self.text = text
        self.imageUrl = imageUrl
        self.expanded = expanded
    }
}
