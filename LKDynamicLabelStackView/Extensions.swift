//
//  Extensions.swift
//  LKDynamicLabelStackView
//
//  Created by Liron Karadi on 20/02/2018.
//

import UIKit

extension UIImageView {
    
    public func imageFromUrl(urlString: String) {
        if let url = NSURL(string: urlString) {
            let request = URLRequest(url: url as URL)
            NSURLConnection.sendAsynchronousRequest(request, queue: .main, completionHandler: { (res, data, error) in
                if let imageData = data as Data? {
                    self.image = UIImage(data: imageData)
                }
            })
        }
    }
}

