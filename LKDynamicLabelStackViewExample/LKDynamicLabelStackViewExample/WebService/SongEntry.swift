//
//  SongEntry.swift
//  eBayCodeChallange
//
//  Created by Liron Karadi on 07/02/2018.
//  Copyright © 2018 eBay. All rights reserved.
//

import ObjectMapper

class BaseSongObject: NSObject, Mappable {
    
    var label: String?
    var attributes: [String : String]?
    
    override init() {
        
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        label <- map["label"]
        attributes <- map["attributes"]
    }
}

class SongsWrapper: BaseSongObject {
    var entries: [SongEntry]?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        entries <- map["entry"]
    }
}

class SongEntry: BaseSongObject {
    var title: SongTitle?
    var images: [SongImage]?
    var price: SongPrice?
    var rights: SongRights?
    var artist: SongArtist?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        title <- map["title"]
        rights <- map["rights"]
        images <- map["im:image"]
        price <- map["im:price"]
        artist <- map["im:artist"]
    }
}

class SongTitle: BaseSongObject {
    
}

class SongImage: BaseSongObject {

}

class SongPrice: BaseSongObject {

}

class SongArtist: BaseSongObject {
    
}

class SongRights: BaseSongObject {
    
}
