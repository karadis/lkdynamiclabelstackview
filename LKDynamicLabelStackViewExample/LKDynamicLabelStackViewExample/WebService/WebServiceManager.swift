//
//  WebServiceManager.swift
//  LKDynamicLabelStackView_Example
//
//  Created by Liron Karadi on 19/02/2018.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit

class WebServiceManager: NSObject {

    static let sharedInstance : WebServiceManager = WebServiceManager()

    func fetchSongItems(success:@escaping(_ items:[SongEntry]?) -> Void, failure:@escaping (_ error: String?) -> Void) {
        
        let url = URL(string: "https://itunes.apple.com/us/rss/topsongs/limit=10/json")
        
        let task = URLSession.shared.dataTask(with: url!) { data, response, error in
            guard error == nil else {
                return failure(error?.localizedDescription)
            }
            guard let data = data else {
                return failure("No data")
            }
            
            let json = try! JSONSerialization.jsonObject(with: data, options: []) as? [String : Any]
            if let feed = json?["feed"] as? [String : Any] {
                let wrapper = SongsWrapper(JSON: feed)
                return success(wrapper?.entries)
            } else {
                return failure("No feed")
            }
        }
        
        task.resume()
    }
    
}
