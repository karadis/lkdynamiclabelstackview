//
//  ViewController.swift
//  eBayCodeChallange
//
//  Created by Liron Karadi on 07/02/2018.
//  Copyright © 2018 eBay. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var stackView: LKDynamicLabelStackView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.stackView = LKDynamicLabelStackView()
        self.stackView?.frame = self.view.bounds
        self.stackView?.color = UIColor.purple
        self.view.addSubview(self.stackView!)

        WebServiceManager.sharedInstance.fetchSongItems(success: { (songs) in
            self.stackView?.items = self.prepareStackItems(songs)
        }) { (error) in
            
        }
    }
    
    func prepareStackItems(_ songs: [SongEntry]? = []) -> [StackItem] {
        var items: [StackItem] = []
        for song in songs! {
            let title = song.title?.label ?? ""
            let artist = song.artist?.label ?? ""
            let price = song.price?.label ?? ""
            let rights = song.rights?.label ?? ""
            let description = "\(title)\n\(artist)\n\(price)\n\(rights)"
            items.append(StackItem(text: description, imageUrl: song.images?.first?.label))
        }
        return items
    }
}

